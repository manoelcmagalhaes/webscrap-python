#necessario instalar as bibliotecas lxml, requests e BeautifulSoup (comando: pip install lxml requests bs4)
#fontes de estudo: (substring) https://stackoverflow.com/questions/3437059/does-python-have-a-string-contains-substring-method
#fontes de estudo: (string format) https://www.w3schools.com/python/ref_string_format.asp
#fontes de estudo: (webscrap) https://stackoverflow.com/questions/2081586/web-scraping-with-python
#fontes de estudo: (download http) https://stackoverflow.com/questions/22676/how-to-download-a-file-over-http
#fontes de estudo: (criação de arquivo) https://www.w3schools.com/python/python_file_write.asp

import urllib.request
import requests
from bs4 import BeautifulSoup

url = "https://mirrors.fy1m.com/h_tools/" #pagina a ser processada
extensao = ".msi" #tipo de arquivo a ser baixado da pagina

page = BeautifulSoup(requests.get(url).text, "lxml") #a variavel page vai receber o conteudo html da pagina a ser processada
contador = 0 #contador para indicar o numero do arquivo sendo processado
links = [a['href'] for a in page.select('a[href]')] #gera um array com todos os hrefs da pagina
for link in links:
    if link.find(extensao) != -1: #verifica se o arquivo tem a extensao informada
        print(f"Processando o arquivo numero {contador}: {link}"); #imprime os dados sobre o arquivo em processamento
        response = urllib.request.urlopen(url+link) #armazena o conteudo do arquivo na variavel response
        arquivo = response.read() #le os dados obtidos e transfere para a variavel arquivo
        f = open(link, "wb") #cria um arquivo em modo binario
        f.write(arquivo) #grava os dados obtidos no arquivo
        f.close() #fecha o arquivo
    else:
        print(f"O arquivo numero {contador} não possui a extensao \"{extensao}\"");
    contador = contador + 1
    