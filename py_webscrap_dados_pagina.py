#necessario instalar as bibliotecas lxml, requests e BeautifulSoup (comando: pip install lxml requests bs4)
#fontes de estudo: (substring) https://stackoverflow.com/questions/3437059/does-python-have-a-string-contains-substring-method
#fontes de estudo: (string format) https://www.w3schools.com/python/ref_string_format.asp
#fontes de estudo: (webscrap) https://stackoverflow.com/questions/2081586/web-scraping-with-python
#fontes de estudo: (download http) https://stackoverflow.com/questions/22676/how-to-download-a-file-over-http
#fontes de estudo: (criação de arquivo) https://www.w3schools.com/python/python_file_write.asp
#fontes de estudo: (webscrap com css selectors) https://www.dataquest.io/blog/web-scraping-python-using-beautiful-soup/

import urllib.request
import requests
from bs4 import BeautifulSoup

url = "https://pt.wikipedia.org/wiki/Wikip%C3%A9dia:P%C3%A1gina_principal" #url a ser processada

page = BeautifulSoup(requests.get(url).text, "html.parser") #a variavel page vai receber o conteudo html da pagina a ser processada
contador = 0 #contador para indicar o numero do arquivo sendo processado
for link in page.find_all('p'): #encontra todos as tags <p>
    print("find_all('p'): "+link.text)
    
    
for link2 in page.select('.nomobile a'):
    print("select('.nomobile a'): "+link2.text)